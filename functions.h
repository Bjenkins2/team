/*************************************************************************//**
 * @file
 *****************************************************************************/
#ifndef __FUNCTIONS_H_
#define __FUNCTIONS_H_

#include <iostream>
#include <fstream>

using namespace std;

/*!
* @brief used for chars that hold parts of the file 
*/
typedef unsigned char binary;

int get_directory();
void compute_images(binary binary_data[], int height, int width, int image_type, int filesize, char filename[], ifstream & in);
int is_gif(ifstream & in);
int is_bmp(ifstream & in);
int is_png(ifstream & in);
int is_jpg(ifstream & in);
int rename_file(char filename[], int image_type, int height, int width);
int convertto_string(int height, int width, char widthandheight[]);

#endif