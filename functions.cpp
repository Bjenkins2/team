#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#include "functions.h"
#include <direct.h>
#include <io.h>
#include <vector>
#include <string>
#include <sstream>

/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function changes directories and figures out if a file or a another directory
 * is being looked at. It then puts the file name into an array and gets the file size
 * and the information inside the file. Then it calls other function to finish processing
 * the file. and repeats
 *
 * @returns 0 function is done
 *
 *****************************************************************************/
int get_directory()
{
    string current_file;
    int filesize = 0;
    int height = 0;
    int i = 0;
    int j = 0;
    int image_type = 0;
    ifstream in;
    int width = 0;
    ofstream out;

    //gets directory contents
    _finddata_t a_file;
    intptr_t  dir_handle;
    dir_handle = _findfirst("*.*", &a_file);

    if (dir_handle == -1)
        return -1;

    do
    {
        //skips sub-directories
        if (!(a_file.attrib & _A_SUBDIR))
        {
            j++; //used to count files

            //opens files in binary mode
            in.open(a_file.name, ios::in | ios::binary);

            //checks if file open successful
            if (in)
            {
                //creates an array for binary data
                binary binary_data[10]; // = new char[filesize];

                //creates array for filename
                char* filename = new char[80]; 

                //puts name of file into filename array
                for (i = 0; i < 80; i++)
                {
                    filename[i] = a_file.name[i];
                }

                //gets image type and width and hieght then renames file
                compute_images(binary_data, height, width, image_type, filesize, filename, in);

            }
            else
                //the file did not open so the name of that file is displayed
                cout << "Error file: " << a_file.name << " did not open" << endl;
        }
        //used to close file when image was not found
        in.close();

    } while (_findnext(dir_handle, &a_file) == 0);
    _findclose(dir_handle);

    if (j == 0)
        cout << "No files in directory" << endl;//no files counted 

    return 0;
}

/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function calls other function to see if the file is an image file.
 * If the file is an image then it is assigned an image type number.  this 
 * number determines how the file is going to be renamed. then the width and
 * the height are computed. After that the file is renamed and the function 
 * is done until the next file is processed.
 *
 * @param[in]  binary_data[] -  the data form the binary file  
 *
 * @param[in]  height - the height of the image
 *
 * @param[in]  width - the width of the image
 *
 * @param[in]  image_type - the type of image
 *
 * @param[in]  filesize - the size of the file
 *
 * @param[in]  filename[] - the name of the file being fixed
 *
 * @param[in]  in - the variable for the file that is open
 *
 *
 *****************************************************************************/
void compute_images(binary binary_data[], int height, int width, int image_type, int filesize, char filename[], ifstream& in)
{
    int i = 0;

    //finds width and height of gif then renames file
    if (is_gif(in) == 1)
    {
        image_type = 1;
        //seeks to the 7 bit(count from 0)
        in.seekg(6, ios::beg);
        for (i = 0; i < 4; i++)
            in >> binary_data[i];
        
        for (i = 0; i <= 1; i++)
        {
            width = binary_data[1 - i] | width << 8;
            height = binary_data[3 - i] | height << 8;
        }
        in.close();
        rename_file(filename, image_type, height, width);
        
    }
    //finds width and height of bmp then renames file
    else if (is_bmp(in) == 2)
    {
        image_type = 2;
        //seeks to the 19 bit(count from 0)
        in.seekg(18, ios::beg);
        for (i = 0; i < 8; i++)
            in >> binary_data[i];
        i = 0;
        for (i = 0; i <= 3; i++)
        {
            width = binary_data[3 - i] | width << 8;
            height = binary_data[7 - i] | height << 8;
        }
        in.close();
        rename_file(filename, image_type, height, width);
    }
    //finds width and height of png then renames file
    else if (is_png(in) == 3)
    {
        image_type = 3;
        //seeks to the 17 bit(count from 0)
        in.seekg(16, ios::beg);
        for (i = 0; i < 8; i++)
            in >> binary_data[i];

        for (i = 0; i <= 3; i++)
        {
            width = binary_data[i] | width << 8;
            height = binary_data[i + 4] | height << 8;
        }
        in.close();
        rename_file(filename, image_type, height, width);
    }
    //renames jpg file
    else if (is_jpg(in) == 4)
    {
        image_type = 4;
        in.close();
        rename_file(filename, image_type, height, width);
    }
    else
        //the file is not an image so theres nothing to process
        ; 
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function checks to see that the first 6 bits in the file is either GIF87a
 * or GIF89a. If there is a match then the function returns a 1. If there is no
 * match then a 0 is returned.
 *
 * @param[in]  in - the file being repaired
 *
 * @returns 0 not a gif
 * @returns 1 is a gif
 *
 *****************************************************************************/
int is_gif(ifstream & in)
{
   char a[2] = "a";
   binary binary_data[8] = "";
   char num1[3] = "89";
   char num2[3] = "87";
   char gif[4] = "GIF";

   //seeks to begining of file
   in.seekg(0, ios::beg);

   //gets data from file to compare
   in.read( (char*)binary_data, 7);

   //GIF87a
   if (binary_data[0] == gif[0] && binary_data[1] == gif[1] && binary_data[2] == gif[2])
   {
       if (binary_data[3] == num1[0] && binary_data[4] == num1[1])
       {
           if (binary_data[5] == a[0])
           {
               return 1;
           }
       }
   }
   //GIF89a
   if (binary_data[0] == gif[0] && binary_data[1] == gif[1] && binary_data[2] == gif[2])
   {
       if (binary_data[3] == num2[0] && binary_data[4] == num2[1])
       {
           if (binary_data[5] == a[0])
           {
               return 1;
           }
       }
   }
   
   return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function check to see if the image being processed is a bmp file. 
 * To do that the funciton checks to see if the fisrt to bits are "BM".  If
 * they are the function returns a 2 if not it returns a 0.
 *
 * @param[in]  in - the file being repaired
 *
 *
 * @returns 0 not a bmp image
 * @returns 2 is a bmp image
 *
 *****************************************************************************/
int is_bmp(ifstream & in)
{
    binary binary_data[4] = "";
    char bm[3] = "BM";
    int i = 0;
    int j = 0;

    //seeks to begining of file
    in.seekg(0, ios::beg);

    //reads in data to comapre
    in.read((char*)binary_data, 2);

    //looks for BM in binary_data[]
    for (i = 0; i < 2; i++)
    {
        //adds one for everytime theres a match
        if (binary_data[i] == bm[i])
            j++;
    }

    //returns 2 if B and M are found
    if (j == 2)
    {
        return 2;
    }
    else
        return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function checks that the numbers 137, 80, 78, 71, 13, 10, 26, 10 are the
 * first 8 bits in the file. If they are then the function returns a 3. If not
 * then the function returns a 0.
 *
 * @param[in]  in - the file being repaired
 * 
 *
 * @returns 0 not a png
 * @returns 3 is a png
 *
 *****************************************************************************/
int is_png(ifstream & in)
{
    binary binary_data[9] = "";
    binary png_nums[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };
    int i = 0;
    int j = 0;

    //seeks to begining of file
    in.seekg(0, ios::beg);

    //gets the first 8 numbers
    in.read((char*)binary_data, 8);

    //loops to find png numbers in binary_data[]
    for (i = 0; i < 9; i++)
    {
        //adds one for every match
        if (binary_data[i] == png_nums[i])
            j++;

    }
    //if all 8 are found then 3 is returned
    if (j == 8)
    {
        return 3;
    }
    else
        return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function checks to see if an image is a jpg by checking if the first
 * two numbers are 255 and 216 and that the last two numbers are 255 and 217.
 * If the function is a jpg then the function returns a 4. If not then 0.
 *
 * @param[in]  in - the file being repaired
 *
 *  
 *                        
 *
 * @returns 0 not a jpg
 * @returns 4 is a jpg
 *
 *****************************************************************************/
int is_jpg(ifstream & in)
{
    binary binary_data[4] = "";
    binary jpg_1[2] = { 255, 216 };
    binary jpg_2[2] = { 255, 217 };
    int i = 0;
    
    //seeks to beggining of file
    in.seekg(0, ios::beg);
   
    //takes information needed to comapre numbers
    for (i = 0; i < 2; i++)
        in >> binary_data[i];

    //looks for numbers 255, 216 at begining of file
    if ((binary_data[0] == jpg_1[0]) && (binary_data[1] == jpg_1[1]))
    {
        //seeks to end of file then goes back 2
        in.seekg(-2, ios::end);

        //takes information needed to comapre numbers
        for (i = 0; i < 2; i++)
            in >> binary_data[i];

        //looks for numbers 255, 217 at end of file. returns 4 if found
        if ((binary_data[0] == jpg_2[0]) && (binary_data[1] == jpg_2[1]))
        {
            return 4;
        }
    }
    return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function calls the convert to char function the get width and height.
 * Then renames the file
 *
 * @param[in]  filename[] - name of the file being fixed    
 *   
 * @param[in]  image_type - type of image being fixed
 *
 * @param[in]  height - the hieght of the image
 *
 * @param[in]  width - the width of the image
 * 
 * @returns 0 program ran successful.
 * @returns 5 file could not be renamed
 *
 *****************************************************************************/
int rename_file(char filename[], int image_type, int height, int width)
{
    char jpg[5] = ".jpg";
    char png[5] = ".png";
    char bmp[5] = ".bmp";
    char gif[5] = ".gif";
    char* widthandheight = new char[80];
    char* newfile_name = new char[80];
    char* oldfile_name = new char[80];
    
    //adds width and height and exstension to gif file
    if (image_type == 1)
    {
        convertto_string(height, width, widthandheight);
        strcpy(newfile_name, filename);
        strcat(newfile_name, widthandheight);
        strcat(newfile_name, gif);
        strcpy(oldfile_name, filename);
    }
    //adds width and height and exstension to bmp file
    else if (image_type == 2)
    {
        convertto_string(height, width, widthandheight);
        strcpy(newfile_name, filename);
        strcat(newfile_name, widthandheight);
        strcat(newfile_name, bmp);
        strcpy(oldfile_name, filename);
    }
    //adds width and height and exstension to png file
    else if (image_type == 3)
    {
        convertto_string(height, width, widthandheight);
        strcpy(newfile_name, filename);
        strcat(newfile_name, widthandheight);
        strcat(newfile_name, png);
        strcpy(oldfile_name, filename);
    }
    else
    {
        //adds exstension to jpg file
        strcpy(newfile_name, filename);
        strcat(newfile_name, jpg);
        strcpy(oldfile_name, filename);       
    }
    //renames the file
    if (rename(oldfile_name, newfile_name) == 0)
    {
        return 0;
    }
    else
        return 5;

    return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This functions turns the int width and int height into a string then 
 * into a char[]. The code to do this was found of the stackoverflow forums.
 *
 *
 * @param[in]  width - the width of the image 
 *                        
 * @param[in]  height - the height of the image
 *
 * @param[in, out]  widthandheight[] - for the rename funciton. 
 *
 *
 * @returns 0 program ran successful.
 * 
 *
 *****************************************************************************/

int convertto_string(int height, int width, char widthandheight[])
{
  
    char and[5] = "_x_";
    char space[2] = "_";
    int i = 0;
    int j = 0;
    int size = 0;
    int size2 = 0;
    
    //turns width int into string
    string width1 = static_cast<ostringstream*>(&(ostringstream() << width))->str();

    //turns height int into string
    string height1 = static_cast<ostringstream*>(&(ostringstream() << height))->str();

    //gets size of width and height string
    size = width1.size() + 1;
    size2 = height1.size() + 1;

    //creates temporary arrays to store width and height
    char* temp1 = new char[size];
    char* temp2 = new char[size2];

    //turns width string into char[]
    for (i = 0; i < size; i++)
    {
        temp1[i] = width1[i];

    }
    //turns height string into char[]
    for (i = 0; i < size2; i++)
    {
        temp2[i] = height1[i];

    }
    //creates tempoary char[] to store width_x_height
    char* temp3 = new char[size + size2 + 5];

    //copies a space in
    strcpy(temp3, space);
    //adds width into temp3
    strcat(temp3, temp1);
    //adds _X_ to temp3
    strcat(temp3, and);
    //adds height to temp3
    strcat(temp3, temp2);
    //copies width_X_height into widthandheight char{}
    strcpy(widthandheight, temp3);
    
    return 0;
}