/*************************************************************************//**
 * @file
 *
 * @mainpage Prog1.cpp
 *
 * @section course_section Course Information
 *
 * @author Brian Jenkins
 *
 * @date 10/10/2014
 *
 * @par Professor:
 *         Roger Schrader
 *
 * @par Course:
 *         CSC 250
 *
 * @par Location:
 *         Classroom Bulding Room 107
 *
 * @section program_section Program Information
 *
 * @details This program fixes file which have had their extensions deleted
 * The program changes the directory that the user puts in (can have multiple directories).
 * The program looks into each file and figures out what kind it is. Then the program 
 * gets the width and the height. After that the file is renamed accordingly 
 * _width_X_height.extension. If the file is not an image then nothing is done.
 * The files that this program can repair are bmp,png,gif, and jpg.
 *
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      
 *
 * @par Usage:
   @verbatim
   c:\> prog1.exe dir2 dir2....dir#
   d:\> c:\bin\prog1.exe
   @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug none
 * no know bugs
 * @todo none
 *
 * @par Modifications and Development Timeline:
   @verbatim
   Date           Modification
   -------------  -------------------------------------------------------------
   Sep 25, 2014  Got command line and directory changer working
   Sep 27, 2014  is_filetype working
   Sep 29, 2014  width and height working
   Oct 3, 2014  rename function working
   Oct 5, 2014  convert int to string working
   Oct 6, 2014 documentation and organization
   Oct 12, 2014 Bug fixes and documentation

   @endverbatim
 *
 *****************************************************************************/
#include <iostream>
#include <fstream>
#include <direct.h>
#include <io.h>
#include <cstring>
#include "functions.h"

using namespace std;

/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function gets the command line arguments and passes it into the 
 * get_directory function. If not enough command line arguments are given 
 * then the program displays an error message and ends the program.
 * If the directory could not be changed then the current path and an
 * error message is displayed.
 *
 * @param[in]      argc - a count of the command line arguments used to start
 *                        the program.
 * @param[in]     argv - a 2d character array of each argument.  Each token
 *                        occupies one line in the array.
 *
 * @returns 0 program ran successful.
 * @returns 1 command line arguments were not correctly given
 *
 *****************************************************************************/
int main(int argc, char**argv)
{
    ofstream out;
    int i = 1;
    char *buffer;
    char *startingpath = _getcwd(NULL, 0);

    //makes sure sufficient command line arguments are given
    if (argc < 2)
    {
        cout << "no command line arguments given" << endl;
        return 1;
    }
   
    //goes through each directory
    for (i = 1; i < argc; i++)
    {
        if (_chdir(argv[i]) == 0)
        {    
            cout << "Program changed directories successfully" << endl;

            //goes into the directory to process files
            get_directory();

            //changes directory back to starting path
            _chdir(startingpath);
        }
        //show current path and tells user that directories could not be changed
        else
        {
            cout << "Unable to change to the directory " << argv[i] << endl;
            buffer = _getcwd(NULL, 0);
            cout << buffer << endl;
            delete[] buffer;
        }
    }
    return 0;
}